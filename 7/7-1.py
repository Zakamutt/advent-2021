def arith_sum(num): # technically the arithmetic sum starting at 1 with step 1 but anyway
    return ((num + 1) * num) / 2

f = open("input.txt")

list = [int(i) for i in f.readline().split(",")]

print(max(list)) # arguably I could have fed this to the range expression below
# there's no way it's efficient to go below 0 or beyond the max number, and after that we can just bruteforce

min = 999999999 # I tried something fancier with None types and a tuple and shit at first. 
                # lost me time to annoying typeerrors despite my control flow being exhaustive I hate python
                # not that I had a chance for the leaderboard anyway lol
index = 0

for i in range(1859): # hardcoded max value as found earlier
    count = 0
    for crab in list:
        count += arith_sum(abs(crab - i)) # remove call to arith_sum to solve problem 1
    if min > count:
        min = count
        index = i # I thought I might need this for part 2, but no

print("min =" + str(min))