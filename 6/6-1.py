f = open("input.txt")

list = [int(i) for i in f.readline().split(",")]


for i in range(80):
    for (index, fish) in enumerate(list):
        if fish == 0:
            list[index] = 6
            list.append(9) # since appended they will always be decreased after all non new ones have been processed
        else:
            list[index] -= 1

print(len(list))