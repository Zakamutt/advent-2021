import numpy as np

# complexity is a bitch and the naive model takes too long for 256 days, 
# so let's think efficiently now

def iterate(matrix):
    zero_count = matrix[0] 
    for i in range(1, 9, 1):
        matrix[i-1] = matrix[i]
    matrix[6] += zero_count
    matrix[8] = zero_count # important to overwrite here

f = open("input.txt")

list = [int(i) for i in f.readline().split(",")]

# used to have dtype='object' here because code was wrong and thus overflowed even int64. actually worked ok
matrix = np.zeros(9, dtype=np.int64)

for fish in list:
    matrix[fish] += 1

for i in range(256):
    iterate(matrix)

print(sum(matrix))