# also solves 2

def read_board(f):

    if f.readline() == "": # skip empty line, check for eof
        return (False, None)
    board = [[int(n) for n in f.readline().split(" ") if not n == ""] for i in range(5)]
    return (True, board)

def check_bingo(board):
    
    for i in range(5):
        vertical_bingo = True
        horizontal_bingo = True
        # we only have bingo if a line or column has been entirely erased in favor of Nones
        for j in range(5):
            if board[i][j] is not None:
                horizontal_bingo = False
            if board[j][i] is not None:
                vertical_bingo = False
        if vertical_bingo or horizontal_bingo:
            return True
    return False

def strike_number(board, number):
    for (index, line) in enumerate(board):
       board[index] = [None if n == number else n for n in line]
    return board

def sum_board(board):
    count = 0
    for line in board:
        for number in line:
            if not number is None:
                count += number
    return count

f = open("input.txt")

bingo_numbers = [int(i) for i in f.readline().split(",")]

boards = []

while (True):
    (flag, board) = read_board(f)
    if not flag:
        break
    boards.append(board)

print("board reading finished")
winners = []

for number in bingo_numbers:
    for (num, board) in enumerate(boards):
        boards[num] = strike_number(board, number)
        if num not in winners:
            if check_bingo(boards[num]):
                winners.append(num)
                print("winning sum: " + str(sum_board(board)) + " bingo number: " + 
                str(number) + " answer: " + str(sum_board(board) * number) + " number:" + str(num))