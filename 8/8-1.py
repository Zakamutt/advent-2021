def parse(line):
    # the digits aren't sorted the same as the patterns, so I have to do a sort
    list = [''.join(sorted(element)) for element in line.strip().split(" ") if not element == "|"]
    return (list[0:10], list[10:14])

# technically since I sort the strings I could maybe make this more efficient
def shared_elements(list1, list2):
    count = 0
    for element in list1:
        if element in list2: count += 1
    return count

# this function kind of ended up being a bit big teehee
def mapping(patterns):
    mapping = [None for i in range(10)]
    for sequence in patterns:
        if len(sequence) == 2: # 1
            mapping[1] = sequence
            continue
        if len(sequence) == 3: # 7
            mapping[7] = sequence
            continue
        if len(sequence) == 4: # 4
            mapping[4] = sequence
            continue
        if len(sequence) == 7: # 8
            mapping[8] = sequence

    # task 2 mappings below, comment out for answer to 1

    for sequence in patterns:
        if len(sequence) == 6: # either {6}, {0}, or {9}. {9} shares an extra segment with {4}, 3 (c)
            if shared_elements(sequence, mapping[4]) == 4:
                mapping[9] = sequence
            elif shared_elements(sequence, mapping[1]) == 2: # {0} shares one more segment with {1}
                mapping[0] = sequence
            else:
                mapping[6] = sequence # other options have been exhausted

    # we have digits 0146789, segments a-c-e--
    # remaining 235, all length 5

    for sequence in patterns:
        if len(sequence) == 5:
            if shared_elements(sequence, mapping[1]) == 2: # only {3} does this
                mapping[3] = sequence
            elif shared_elements(sequence, mapping[9]) == 5: # {5} shares 2 more
                mapping[5] = sequence
            else:
                mapping[2] = sequence

    return mapping

f = open("input.txt")

count = 0
values = 0
for line in f:
    (patterns, digits) = parse(line)
    mappings = mapping(patterns)
    for digit in digits:
        if digit in mappings:
            count += 1
    numbers = ""
    # really should have made a dictionary instead. but too mendou now
    for digit in digits:
        for i in range(10):
            if digit == mappings[i]:
                numbers += str(i)
    values += int(numbers)
            
print("number of digits found: " + str(count))
print("total value: " + str(values))