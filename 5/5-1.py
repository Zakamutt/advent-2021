import numpy as np

def parse(line):
    # python doesn't support multiple delimiters for split so here we are doing 2n of work
    # not that 2n of work is anything, we're using python
    list = [int(i) for i in line.replace(",", " ").split(" ") if not i == "->"]
    x1 = list[0]
    y1 = list[1]
    x2 = list[2]
    y2 = list[3]
    return (x1, y1, x2, y2)

f = open("input.txt")

# create [1000][1000] matrix. the default is float(?), hopefully this is faster (and more sane)
# the code was very slow earlier for a different reason (it was wrong) so I decided I wanted a REAL ARRAY
matrix = np.zeros((1000, 1000), dtype=np.int32)

for line in f:
    (x1, y1, x2, y2) = parse(line)
    xmax = max(x1, x2)
    ymax = max(y1, y2)
    ydiff = abs(y1 - y2)
    xdiff = abs(x1 - x2)
    
     # you can actually swap x and y in the matrix assignments below (all!) and the matrices
     # will display properly in vscode debug (the matrices are square so it doesn't matter or something)
     # iteration methods used are different but reflects the first and second solutions I used
    if ydiff == 0:
        for x in range(xmax - xdiff, xmax + 1):
            matrix[x][y1] += 1
            continue
    if xdiff == 0:
        for y in range(ymax - ydiff, ymax + 1):
            matrix[x1][y] += 1
            continue
    if (xdiff == ydiff): # diagonal, comment out for answer to q1
        iter_x = 1
        iter_y = 1
        if x1 > x2:
            iter_x = -1
        if y1 > y2:
            iter_y = -1
        # travel from (x1, y1) to (x2, y2) in steps of 1 or -1 depending on which direction to go
        iter = zip(range(x1, x2 + iter_x, iter_x), range(y1, y2 + iter_y, iter_y))
        for (x, y) in iter:
            matrix[x][y] += 1

count = 0
# numpy.nditer iterates over every element once in any order
for i in np.nditer(matrix):
    if i > 1:
        count += 1
print("list count: " + str(count))