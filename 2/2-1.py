f = open("input.txt")

fwd = 0
depth = 0

for line in f:
    (direction, amt) = line.split(" ")
    if direction == "forward":
        fwd += int(amt)
    elif direction == "down":
        depth += int(amt)
    elif direction == "up":
        depth -= int(amt)
    else:
        print("wtf unhandled case")

print("fwd = " + str(fwd))
print("depth = " + str(depth))
print("ans = " + str(fwd * depth))