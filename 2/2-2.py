f = open("input.txt")

fwd = 0
depth = 0
aim = 0

for line in f:
    (direction, amt) = line.split(" ")
    if direction == "forward":
        fwd += int(amt)
        depth += int(amt) * aim
    elif direction == "down":
        aim += int(amt)
    elif direction == "up":
        aim -= int(amt)
    else:
        print("wtf unhandled case")

print("fwd = " + str(fwd))
print("depth = " + str(depth))
print("ans = " + str(fwd * depth))