f = open("input.txt")

counter = 0
wind = [None for i in range(0, 4)]

wind[0] = int(f.readline())
wind[1] = int(f.readline())
wind[2] = int(f.readline())
wind[3] = int(f.readline())

while (f):
    if wind[0] + wind[1] + wind[2] < wind[1] + wind[2] + wind[3]:
        counter += 1
    tmp = f.readline()
    if not tmp:
        break

    wind[0] = wind[1]
    wind[1] = wind[2]
    wind[2] = wind[3]
    wind[3] = int(tmp)
    

print(counter)
f.close()