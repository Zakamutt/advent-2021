def scan(list, index):
    count = 0
    for line in list:
        if line[index] == "1":
            count += 1
        elif line[i] == "0":
            count -= 1
        else:
            print("wtf unhandled case")
    return count

def filter_list(list, index, number):
    return [i for i in list if i[index] == str(number)]

f = open("input.txt")

list = [line for line in f]
list_copy = list.copy()

for i in range(12):
    if len(list_copy) == 1:
        break
    tmp = scan(list_copy, i)
    if tmp >= 0: # 1 when equal
        tmp = 1
    else:
        tmp = 0
    list_copy = filter_list(list_copy, i, tmp)

# I was too lazy to transform the solutions from binary in-code I used an online converter

print("oxygen gen rating:")
print(list_copy)

list_copy = list

for i in range(12):
    if len(list_copy) == 1:
        break
    tmp = scan(list_copy, i)
    if tmp < 0: # 1 when equal
        tmp = 1
    else:
        tmp = 0
    list_copy = filter_list(list_copy, i, tmp)

print("co2 rating:")
print(list_copy)