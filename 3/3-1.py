f = open("input.txt")

vals = [0 for i in range(12)]

# python is horrible for thinking about things on the bit level, so
# I'm just doing this the max cringe way and reading shit as strings
# (I could probably had done more still but my brain died and didn't realize
# I could treat a positive 2s complement int as normal bitwise)

for line in f:
    for i in range(12):
        if line[i] == "1":
            vals[i] += 1
        elif line[i] == "0":
            vals[i] -= 1
        else:
            print("wtf unhandled case")

print(vals)
gamma = 0
epsilon = 0

vals.reverse() # in case instructions weird (they were, or my brain lagged idk)

for i in range(12):
    if vals[i] > 0:
        gamma += (2 ** i)
    else:
        epsilon += (2 ** i)
print("gamma = " + str(gamma) + " epsilon = " + str(epsilon) + " consumption = " + str(gamma * epsilon))

for i in range(12):
    if vals[i] > 0:
        vals[i] = 1
    else:
        vals[i] = 0
print("final bits, apparently matches epsilon")
vals.reverse()
print(vals)


    